package main

import (
	"errors"
	"fmt"
)

func (s stem) conjugate(
	cs []conjugation,
	conj string,
	person int,
	plural bool,
) (string, error) {
	if person > 2 {
		return "", errors.New("person cannot be greater than 2")
	}

	conjugation := getConjugation(cs, conj)
	if conjugation == nil {
		return "", fmt.Errorf("conjugation %s not found", conj)
	}

	supported := false
	for _, c := range s.Conjugations {
		if c == conj {
			supported = true
		}
	}
	if !supported {
		return "", fmt.Errorf("conjugation %s not supported by stem %v", conj, s.Stem)
	}

	var cases [3]string
	if plural {
		cases = conjugation.Plural
	} else {
		cases = conjugation.Singular
	}

	return conjugateStem(s.Stem, cases[person]), nil
}

func getConjugation(cs []conjugation, name string) *conjugation {
	for _, conj := range cs {
		if conj.Name == name {
			return &conj
		}
	}
	return nil
}

// conjugate stem with case `cas`
func conjugateStem(stem string, cas string) string {
	if cas == "" {
		return stem
	}

	nsub := 0
	for len(cas) > 0 && cas[0] == '-' {
		cas = cas[1:]
		nsub++
	}
	stem = stem[:len(stem)-nsub]

	return stem + cas
}
