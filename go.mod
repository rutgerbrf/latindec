module gitlab.com/rutgerbrf/latindec

require (
	github.com/gocarina/gocsv v0.0.0-20190131101517-2a8c07cdf701
	gopkg.in/yaml.v2 v2.2.2
)
