package main

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path/filepath"
)

type conjugation struct {
	Name          string    `json:"name"`
	Singular      [3]string `json:"singular"`
	Plural        [3]string `json:"plural"`
	HasImperative bool      `json:"hasImperative" yaml:"has-imperative"`
	Imperative    [2]string `json:"imperative"`
	Infinitive    string    `json:"inifinitive"`
}

func loadConjugations() ([]conjugation, error) {
	conjPaths := make([]string, 0)
	conjsPath := filepath.Join(configPath, "conjugations")
	err := filepath.Walk(conjsPath, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			conjPaths = append(conjPaths, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	conjs := make([]conjugation, 0)
	for _, p := range conjPaths {
		bytes, err := ioutil.ReadFile(p)
		if err != nil {
			return nil, err
		}

		var conj conjugation
		err = yaml.Unmarshal(bytes, &conj)
		if err != nil {
			return nil, err
		}

		conjs = append(conjs, conj)
	}

	return conjs, nil
}
