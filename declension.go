package main

import (
	"io/ioutil"
	"os"
	"path/filepath"

	yaml "gopkg.in/yaml.v2"
)

func loadDeclensions() ([]declension, error) {
	declensionPaths := make([]string, 0)
	declensionsPath := filepath.Join(configPath, "declensions")
	err := filepath.Walk(declensionsPath, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			declensionPaths = append(declensionPaths, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	declensions := make([]declension, 0)
	for _, p := range declensionPaths {
		bytes, err := ioutil.ReadFile(p)
		if err != nil {
			return nil, err
		}

		dp := declensionPrototype{}
		err = yaml.Unmarshal(bytes, &dp)
		if err != nil {
			return nil, err
		}

		declensions = append(declensions, dp.toDeclension())
	}

	return declensions, nil
}

type declensionPrototype struct {
	Name     string    `json:"name"`
	Singular [5]string `json:"singular"`
	Plural   [5]string `json:"plural"`
}

func (dp declensionPrototype) toDeclension() declension {
	d := declension{Name: dp.Name}
	d.Singular.loadList(dp.Singular)
	d.Plural.loadList(dp.Plural)
	return d
}

type declension struct {
	Name     string `json:"name"`
	Singular cases  `json:"singular"`
	Plural   cases  `json:"plural"`
}

func (d declension) toPrototype() declensionPrototype {
	return declensionPrototype{
		Name:     d.Name,
		Singular: d.Singular.toList(),
		Plural:   d.Plural.toList(),
	}
}
