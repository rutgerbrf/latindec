package main

import (
	"io/ioutil"
	"os"
	"path/filepath"

	yaml "gopkg.in/yaml.v2"
)

type stem struct {
	Stem         string   `json:"stem"`
	Primary      string   `json:"primary"`
	Conjugations []string `json:"conjugations,omitempty"`
	Declensions  []string `json:"declensions,omitempty"`
}

func loadStems() ([]stem, error) {
	stemPaths := make([]string, 0)
	stemsPath := filepath.Join(configPath, "stems")
	err := filepath.Walk(stemsPath, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			stemPaths = append(stemPaths, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	stems := make([]stem, 0)
	for _, p := range stemPaths {
		bytes, err := ioutil.ReadFile(p)
		if err != nil {
			return nil, err
		}

		s := make([]stem, 0)
		err = yaml.Unmarshal(bytes, &s)
		if err != nil {
			return nil, err
		}

		stems = append(stems, s...)
	}

	return stems, nil
}
