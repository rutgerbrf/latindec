package main

import (
	"encoding/json"
	"fmt"
	"github.com/gocarina/gocsv"
	"io/ioutil"
	"os"
)

const configPath = "config"

func main() {
	declensions := processDeclensions()
	conjugations := processConjugations()
	stems := processStems()

	declinedStems := declineStems(stems, declensions)
	conjugatedStems := conjugateStems(stems, conjugations)

	writeDeclinedStems(declinedStems)
	writeConjugatedStems(conjugatedStems)
}

func processStems() []stem {
	ss, err := loadStems()
	if err != nil {
		fmt.Printf("Error loading stems: %v\n", err)
		os.Exit(1)
	}

	stemBytes, err := json.MarshalIndent(ss, "", "    ")
	if err != nil {
		fmt.Printf("Error mashalling stems: %v\n", err)
		os.Exit(1)
	}

	err = ioutil.WriteFile("stems.json", stemBytes, 0755)
	if err != nil {
		fmt.Printf("Error writing stems: %v\n", err)
		os.Exit(1)
	}

	return ss
}

func processDeclensions() []declension {
	ds, err := loadDeclensions()
	if err != nil {
		fmt.Printf("Error loading declensions: %v\n", err)
		os.Exit(1)
	}

	decBytes, err := json.MarshalIndent(ds, "", "    ")
	if err != nil {
		fmt.Printf("Error marshalling declensions: %v\n", err)
		os.Exit(1)
	}

	err = ioutil.WriteFile("declensions.json", decBytes, 0755)
	if err != nil {
		fmt.Printf("Error writing declensions: %v\n", err)
		os.Exit(1)
	}

	return ds
}

func processConjugations() []conjugation {
	cs, err := loadConjugations()
	if err != nil {
		fmt.Printf("Error loading conjugations: %v\n", err)
		os.Exit(1)
	}

	conBytes, err := json.MarshalIndent(cs, "", "    ")
	if err != nil {
		fmt.Printf("Error mashalling conjugations: %v\n", err)
		os.Exit(1)
	}

	err = ioutil.WriteFile("conjugations.json", conBytes, 0755)
	if err != nil {
		fmt.Printf("Error writing conjugations: %v\n", err)
		os.Exit(1)
	}

	return cs
}

type stemDeclension struct {
	Name   string `json:"name"`
	Result string `json:"result"`
}

type stemConjugation struct {
	Name   string `json:"name"`
	Result string `json:"result"`
}

func pluralityStr(plural bool) string {
	if plural {
		return "plural"
	}
	return "singular"
}

var decCases = []declensionCase{
	nominative,
	genitive,
	dative,
	accusative,
	ablative,
}

func declineStems(stems []stem, declensions []declension) []stemDeclension {
	sds := make([]stemDeclension, 0)
	for _, stem := range stems {
		for _, d := range stem.Declensions {
			declension := getDeclension(declensions, d)
			if declension == nil {
				continue
			}

			for _, cas := range decCases {
				d, err := stem.decline(declensions, declension.Name, cas, false)
				if err != nil {
					fmt.Printf("Error declining: %v\n", err)
				}
				sds = append(sds, stemDeclension{
					Name:   fmt.Sprintf("%s %s %s. decl %s", cas, pluralityStr(false), declension.Name, stem.Stem),
					Result: d,
				})
			}

			for _, cas := range decCases {
				d, err := stem.decline(declensions, declension.Name, cas, true)
				if err != nil {
					fmt.Printf("Error declining: %v\n", err)
				}
				sds = append(sds, stemDeclension{
					Name:   fmt.Sprintf("%s %s %s. decl %s", cas, pluralityStr(true), declension.Name, stem.Stem),
					Result: d,
				})
			}
		}
	}
	return sds
}

func conjugateStems(stems []stem, conjugations []conjugation) []stemConjugation {
	scs := make([]stemConjugation, 0)
	for _, stem := range stems {
		for _, d := range stem.Conjugations {
			declension := getConjugation(conjugations, d)
			if declension == nil {
				continue
			}

			for person := 0; person < 3; person++ {
				d, err := stem.conjugate(conjugations, declension.Name, person, false)
				if err != nil {
					fmt.Printf("Error conjugating: %v\n", err)
				}
				scs = append(scs, stemConjugation{
					Name:   fmt.Sprintf("%d. pers %s %s %s", person, pluralityStr(false), declension.Name, stem.Stem),
					Result: d,
				})
			}

			for person := 0; person < 3; person++ {
				d, err := stem.conjugate(conjugations, declension.Name, person, true)
				if err != nil {
					fmt.Printf("Error conjugating: %v\n", err)
				}
				scs = append(scs, stemConjugation{
					Name:   fmt.Sprintf("%d. pers %s %s %s", person, pluralityStr(true), declension.Name, stem.Stem),
					Result: d,
				})
			}
		}
	}
	return scs
}

func writeDeclinedStems(declinedStems []stemDeclension) {
	dsBytes, err := json.MarshalIndent(declinedStems, "", "    ")
	if err != nil {
		fmt.Printf("Error marshalling declined stems: %v\n", err)
		os.Exit(1)
	}

	err = ioutil.WriteFile("declined-stems.json", dsBytes, 0755)
	if err != nil {
		fmt.Printf("Error writing declined stems: %v\n", err)
		os.Exit(1)
	}

	f, err := os.Create("declined-stems.csv")
	if err != nil {
		fmt.Printf("Error creating declined-stems.csv: %v\n", err)
		os.Exit(1)
	}

	err = gocsv.MarshalFile(declinedStems, f)
	if err != nil {
		fmt.Printf("Error marshalling declined stems to file: %v\n", err)
		os.Exit(1)
	}
}

func writeConjugatedStems(conjugatedStems []stemConjugation) {
	dsBytes, err := json.MarshalIndent(conjugatedStems, "", "    ")
	if err != nil {
		fmt.Printf("Error marshalling conjugated stems: %v\n", err)
		os.Exit(1)
	}

	err = ioutil.WriteFile("conjugated-stems.json", dsBytes, 0755)
	if err != nil {
		fmt.Printf("Error writing conjugated stems: %v\n", err)
		os.Exit(1)
	}

	f, err := os.Create("conjugated-stems.csv")
	if err != nil {
		fmt.Printf("Error creating conjugated-stems.csv: %v\n", err)
		os.Exit(1)
	}

	err = gocsv.MarshalFile(conjugatedStems, f)
	if err != nil {
		fmt.Printf("Error marshalling conjugated stems to file: %v\n", err)
		os.Exit(1)
	}
}
