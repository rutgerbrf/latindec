package main

import "fmt"

type declensionCase string

const (
	nominative declensionCase = "nominative"
	genitive                  = "genitive"
	dative                    = "dative"
	accusative                = "accusative"
	ablative                  = "ablative"
)

func (s stem) decline(
	ds []declension,
	decl string,
	cas declensionCase,
	plural bool,
) (string, error) {
	declension := getDeclension(ds, decl)
	if declension == nil {
		return "", fmt.Errorf("declension %s not found", decl)
	}

	supported := false
	for _, dn := range s.Declensions {
		if dn == decl {
			supported = true
		}
	}
	if !supported {
		return "", fmt.Errorf("declension %s not supported by stem %v", decl, s.Stem)
	}

	var cs *cases
	if plural {
		cs = &declension.Plural
	} else {
		cs = &declension.Singular
	}

	casStr := ""
	switch cas {
	case nominative:
		casStr = cs.Nominative
	case genitive:
		casStr = cs.Genitive
	case dative:
		casStr = cs.Dative
	case accusative:
		casStr = cs.Accusative
	case ablative:
		casStr = cs.Ablative
	}
	return declineStem(s.Stem, s.Primary, casStr), nil
}

func getDeclension(ds []declension, name string) *declension {
	for _, dec := range ds {
		if dec.Name == name {
			return &dec
		}
	}
	return nil
}

// decline stem with case `cas`
func declineStem(stem string, primary string, cas string) string {
	if cas == "" {
		if primary != "" {
			return primary
		}
		return stem
	}

	nsub := 0
	for len(cas) > 0 && cas[0] == '-' {
		cas = cas[1:]
		nsub++
	}
	stem = stem[:len(stem)-nsub]

	return stem + cas
}
