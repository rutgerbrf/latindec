package main

type cases struct {
	Nominative string `json:"nominative"`
	Genitive   string `json:"genitive"`
	Dative     string `json:"dative"`
	Accusative string `json:"accusative"`
	Ablative   string `json:"ablative"`
}

func (cs *cases) loadList(cases [5]string) {
	for i, c := range cases {
		switch i {
		case 0:
			cs.Nominative = c
		case 1:
			cs.Genitive = c
		case 2:
			cs.Dative = c
		case 3:
			cs.Accusative = c
		case 4:
			cs.Ablative = c
		default:
			return
		}
	}
}

func (cs cases) toList() [5]string {
	return [5]string{
		cs.Nominative,
		cs.Genitive,
		cs.Dative,
		cs.Accusative,
		cs.Ablative,
	}
}
